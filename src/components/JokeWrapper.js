import * as React from 'react'
import toast from 'react-hot-toast'
import { useQuery } from 'react-query'
import { motion, AnimatePresence, } from 'framer-motion';

const JokeWrapper = () => {
    const getJoke = async () => {
        const r = await fetch('https://v2.jokeapi.dev/joke/Programming?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=single')
        return r.json()
    }

    const {data:joke} = useQuery('joke',getJoke,{
        onSuccess: res => {
            if(res?.error) {
                toast.error('Došlo je do pogreške na serveru')
                return null
            }
            toast.success('Šala dostavljena :)')
        },
        onError: () => toast.error('Došlo je do pogreške'),
        
    })

    return <AnimatePresence exitBeforeEnter>
        <motion.div>
            <p>{joke?.joke}</p>
        </motion.div>
    </AnimatePresence>
}

export default JokeWrapper