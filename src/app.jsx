import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Index from './Index';
import { QueryClient,QueryClientProvider } from 'react-query';


function render() {
  ReactDOM.render(<React.StrictMode>
      <Index/>
  </React.StrictMode>, document.body);
}

render();